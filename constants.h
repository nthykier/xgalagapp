#ifndef CONSTANTS_H
#define CONSTANTS_H


// Aliens dimension
enum { g_alien_width = 19,
       g_aliens_hspacing = 22,
       g_alien_height = 20,
       g_aliens_vspacing = 24
};


#endif
